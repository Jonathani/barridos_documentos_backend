package com.gob.ec.sercop.lecturadocs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.lecturadocs.daoPostgresMic.ConsultarProcesosDao;
import com.gob.ec.sercop.lecturadocs.dto.ConsultarProcesoRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.ConsultarProcesoResponseDto;
import com.gob.ec.sercop.lecturadocs.service.ConsultarProcesosService;
import com.gob.ec.sercop.utilitarioGeneral.codificacion.TipoDeRegimen;

@Service
public class ConsultarProcesosServiceImpl implements ConsultarProcesosService{
	
	@Autowired
	ConsultarProcesosDao consultarProcesosDao;  
	
	@Override
	public List<ConsultarProcesoResponseDto> listarConsultaProcesos(ConsultarProcesoRequestDto consultarProcesoRequestDto) {
		
		List<ConsultarProcesoResponseDto> listadoRespuesta = new ArrayList<>();
		List<Object[]> listaResultadoNativo = new ArrayList<>();
		listaResultadoNativo = consultarProcesosDao.findByCodigo(consultarProcesoRequestDto.getCodigoProceso().trim());		
		listaResultadoNativo.forEach(
			x -> {
				listadoRespuesta.add(new ConsultarProcesoResponseDto(
						x[0] != null ? Integer.parseInt(x[0].toString()) : 0L, 
						x[1] != null ? x[1].toString() : "", 
						x[2] != null ? x[2].toString() : "", 
						x[3] != null ? Integer.parseInt(x[3].toString()) : 0L, 
						x[4] != null ? x[4].toString() : "",
						x[3] != null ? TipoDeRegimen.obtenerTipoRegimen(Long.parseLong(x[3].toString())): "",
						x[5] != null ? x[5].toString() : "", 
						x[6] != null ? x[6].toString() : "",
						x[7] != null ? x[7].toString() : "", 
						x[8] != null ? x[8].toString() : "", 
						x[9] != null ? x[9].toString() : "", 
						x[10] != null ? x[10].toString() : "", 
						x[11] != null ? x[11].toString() : "", 
						x[12] != null ? x[12].toString() : "", 
						x[13] != null ? x[13].toString() : "",
						x[14] != null ? x[14].toString() : ""
				));
			}
		);		
		
		return listadoRespuesta;
	}

}
