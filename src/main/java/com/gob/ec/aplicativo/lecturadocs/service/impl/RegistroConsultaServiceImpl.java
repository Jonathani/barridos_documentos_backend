package com.gob.ec.sercop.lecturadocs.service.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.lecturadocs.daoPostgres.RegistroConsultaDao;
import com.gob.ec.sercop.lecturadocs.daoPostgres.UsuariosDao;
import com.gob.ec.sercop.lecturadocs.dto.RegistrarConsultaRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.RegistrarConsultaResponseDto;
import com.gob.ec.sercop.lecturadocs.model.postgres.RegistroConsulta;
import com.gob.ec.sercop.lecturadocs.model.postgres.Usuario;
import com.gob.ec.sercop.lecturadocs.service.RegistroConsultaService;

@Service
public class RegistroConsultaServiceImpl implements RegistroConsultaService{

	@Autowired
	UsuariosDao usuariosDao;
	
	@Autowired
	RegistroConsultaDao registroConsultaDao;	
	
	@Override
	public String registrarConsulta(RegistrarConsultaRequestDto registrarConsultaRequestDto) {
		
		String idRegistroConsulta = "";
		Usuario usuario = null;
		LocalDateTime now = LocalDateTime.now();
		Timestamp fechaActual = Timestamp.valueOf(now);
		try {
			usuario = usuariosDao.findByIdUsuario(registrarConsultaRequestDto.getIdUsuario());
			
			RegistroConsulta registroConsulta = new RegistroConsulta();
//			registroConsulta.setPathArchivoJustificacion(rutaArchivo);
//			registroConsulta.setNombreArchivo(nombreArchivo);
			registroConsulta.setCaracteristicas(registrarConsultaRequestDto.getCaracteristicas());
			registroConsulta.setProcedmientos(registrarConsultaRequestDto.getProcedimientos());
			registroConsulta.setUsuario(usuario);
			registroConsulta.setFechaConsulta(fechaActual);
			registroConsultaDao.save(registroConsulta);
			
			idRegistroConsulta =  String.valueOf(registroConsulta.getIdConsulta());
		} catch (Exception e) {
			// TODO: handle exception
		}		
		return idRegistroConsulta;
	}

	@Override
	public List<RegistrarConsultaResponseDto> listarTodosLosRegistrosConsultas() {
		
		List<RegistrarConsultaResponseDto> listaDeRegistroConsultas = new ArrayList<>();
		List<RegistroConsulta> consultasRegistros = new ArrayList<>();
		
		consultasRegistros = registroConsultaDao.findAll();
		for (RegistroConsulta registroConsulta: consultasRegistros) {
			listaDeRegistroConsultas.add(new RegistrarConsultaResponseDto(
					registroConsulta.getUsuario().getIdentificacion(), 
					registroConsulta.getUsuario().getNombreCompleto(), 
					registroConsulta.getProcedmientos(), 
					registroConsulta.getCaracteristicas(), 
					registroConsulta.getFechaConsulta().toString(), 
					registroConsulta.getPathArchivoJustificacion(),
					registroConsulta.getNombreArchivo())
			);
		}
		return listaDeRegistroConsultas;
	}
}
