package com.gob.ec.sercop.lecturadocs;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableJpaRepositories(basePackages = "com.gob.ec.sercop.lecturadocs.daoPostgresMic",
entityManagerFactoryRef = "postgresMicEntityManagerFactory",
transactionManagerRef= "postgresMicTransactionManager")
public class PostgresMicDataSourceConfiguration {
	
	@Bean
	@ConfigurationProperties("spring.datasource.mic")
	public DataSourceProperties postgresMicDatasourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	@ConfigurationProperties("spring.datasource.mic.configuration")
	public DataSource postgresMicDataSource() {
		return postgresMicDatasourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	@Bean(name = "postgresMicEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean postgresMicEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(postgresMicDataSource()).packages("com.gob.ec.sercop.lecturadocs.model.postgresmic")
				.persistenceUnit("mic").build();
	}

	@Bean(name = "postgresMicTransactionManager")
	public PlatformTransactionManager postgresMicTransactionManager(final @Qualifier("postgresMicEntityManagerFactory") LocalContainerEntityManagerFactoryBean postgresMicEntityManagerFactory) {
		return new JpaTransactionManager(postgresMicEntityManagerFactory.getObject());
	}

}
