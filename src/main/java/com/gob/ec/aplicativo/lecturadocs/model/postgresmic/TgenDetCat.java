package com.gob.ec.sercop.lecturadocs.model.postgresmic;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the tgen_det_cat database table.
 * 
 */
@Getter
@Setter
@Entity
@Table(name="tgen_det_cat")
@NamedQuery(name="TgenDetCat.findAll", query="SELECT t FROM TgenDetCat t")
public class TgenDetCat implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TgenDetCatPK id;

	private String descripcion;

	private Boolean estado;	

//	//bi-directional many-to-one association to TcomSolicitudCompra
//	@OneToMany(mappedBy="tgenDetCat1")
//	private List<TcomSolicitudCompra> tcomSolicitudCompras1;
//
//	//bi-directional many-to-one association to TcomSolicitudCompra
//	@OneToMany(mappedBy="tgenDetCat2")
//	private List<TcomSolicitudCompra> tcomSolicitudCompras2;
//
//	//bi-directional many-to-one association to TcomSolicitudCompra
//	@OneToMany(mappedBy="tgenDetCat3")
//	private List<TcomSolicitudCompra> tcomSolicitudCompras3;
//
//	//bi-directional many-to-one association to TcomSolicitudCompra
//	@OneToMany(mappedBy="tgenDetCat4")
//	private List<TcomSolicitudCompra> tcomSolicitudCompras4;
//
//	//bi-directional many-to-one association to TcomSolicitudCompra
//	@OneToMany(mappedBy="tgenDetCat5")
//	private List<TcomSolicitudCompra> tcomSolicitudCompras5;
//
//	//bi-directional many-to-one association to TcomSolicitudCompra
//	@OneToMany(mappedBy="tgenDetCat6")
//	private List<TcomSolicitudCompra> tcomSolicitudCompras6;


}