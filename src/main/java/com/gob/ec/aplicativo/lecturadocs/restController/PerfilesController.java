package com.gob.ec.sercop.lecturadocs.restController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.lecturadocs.service.PerfilService;

@RestController
@RequestMapping("api/perfilesController")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class PerfilesController {
	
	@Autowired
	PerfilService perfilService;
	
	public static final String MENSAJE = "mensaje";
	
	@SuppressWarnings("rawtypes")
	@PostMapping(value = "listarTodosLosPerfiles", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> listarTodosLosPerfiles() {
		
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, List> respuestaTotal = new HashMap<String, List>();
		try {
			respuestaTotal.put(MENSAJE, perfilService.listarPerfilesActivos());			
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, List>>(respuestaTotal, HttpStatus.OK);
		
	}

}
