package com.gob.ec.sercop.lecturadocs.model.postgres;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the usuario database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="usuario")
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
@NamedQuery(name=Usuario.OBTENER_USUARIO_POR_IDENTIFICACION, query="SELECT usuario FROM Usuario usuario WHERE usuario.identificacion = :identificacion and usuario.estado is true")
@NamedQuery(name=Usuario.OBTENER_INFORMACION_DE_USUARIO_POR_IDENTIFICACION, 
				query=" SELECT usuario.idUsuario, COALESCE(usuario.identificacion,''), COALESCE(usuario.nombreCompleto,''), "
						+ " usuario.estado, usuario.estadoContrasenia, COALESCE(usuario.correo,''), "
						+ " perfil.idPerfil, COALESCE(perfil.descripcion,'') "
						+ " FROM Usuario usuario "
						+ " JOIN usuario.perfilUsuarios perfilUsuarios "
						+ " JOIN perfilUsuarios.perfil perfil "
						+ " WHERE usuario.identificacion = :identificacion "
						+ " and usuario.estado is true ")
@NamedQuery(name=Usuario.OBTENER_INFORMACION_DE_TODOS_LOS_USUARIOS, 
			query=" SELECT usuario.idUsuario, COALESCE(usuario.identificacion,''), COALESCE(usuario.nombreCompleto,''), "
					+ " usuario.estado, usuario.estadoContrasenia, COALESCE(usuario.correo,''), "
					+ " perfil.idPerfil, COALESCE(perfil.descripcion,''), usuario.fechaCreacion, usuario.fechaActualizacion, perfilUsuarios.idPerfilUsuario"
					+ " FROM Usuario usuario "
					+ " JOIN usuario.perfilUsuarios perfilUsuarios "
					+ " JOIN perfilUsuarios.perfil perfil ")
@NamedQuery(name=Usuario.OBTENER_INFORMACION_RECUPERAR_CORREO, query="SELECT usuario FROM Usuario usuario WHERE usuario.identificacion = :identificacion and usuario.correo = :correo")
public class Usuario implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static final String OBTENER_USUARIO_POR_IDENTIFICACION = "Usuario.obtenerUsuarioPorIdentificacion";
	
	public static final String OBTENER_INFORMACION_DE_USUARIO_POR_IDENTIFICACION = "Usuario.obtenerInformacionDeUsuarioPorIdentificacion";
	
	public static final String OBTENER_INFORMACION_RECUPERAR_CORREO = "Usuario.obtenerInformacionRecuperarCorreo";
	
	public static final String OBTENER_INFORMACION_DE_TODOS_LOS_USUARIOS = "Usuario.obtenerInformacionDeTodosLosUsuarios";

	@Id
	@SequenceGenerator(name = "usuario_id_seq", sequenceName = "public.usuario_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "usuario_id_seq")
	@Column(name="id_usuario")
	private long idUsuario;

	@Column(name="contrasenia")
	private String contrasenia;

	@Column(name="correo")
	private String correo;

	@Column(name="estado")
	private Boolean estado;

	@Column(name="estado_contrasenia")
	private Boolean estadoContrasenia;

	@Column(name="fecha_actualizacion")
	private Timestamp fechaActualizacion;

	@Column(name="fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name="identificacion")
	private String identificacion;

	@Column(name="nombre_completo")
	private String nombreCompleto;

	//bi-directional many-to-one association to PerfilUsuario
	@OneToMany(mappedBy="usuario")
	private List<PerfilUsuario> perfilUsuarios;

	//bi-directional many-to-one association to RegistroConsulta
	@OneToMany(mappedBy="usuario")	
	private List<RegistroConsulta> registroConsultas;	

}