package com.gob.ec.sercop.lecturadocs.daoPostgres;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.lecturadocs.model.postgres.PerfilUsuario;

public interface PerfilUsuarioDao extends CrudRepository<PerfilUsuario, Long>{
	
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT perfilUsuario "
			+ "FROM PerfilUsuario perfilUsuario "
			+ "where perfilUsuario.idPerfilUsuario = :idPerfilUsuario")
	public PerfilUsuario findByIdPerfilUsuario(long idPerfilUsuario);
	
	
}
