package com.gob.ec.sercop.lecturadocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarContraseniaRequestDto {	
	
	private long idUsuario;
	
	private String contraseniaActual;
	
	private String contraseniaNueva;

	
}
