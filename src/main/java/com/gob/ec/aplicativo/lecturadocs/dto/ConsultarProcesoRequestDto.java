package com.gob.ec.sercop.lecturadocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConsultarProcesoRequestDto {
	
	private String codigoProceso;
	
	private String palabrasBusqueda;

}
