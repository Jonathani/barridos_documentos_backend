package com.gob.ec.sercop.lecturadocs.daoPostgresMic;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gob.ec.sercop.lecturadocs.model.postgresmic.TcomSolicitudCompra;
import com.gob.ec.sercop.lecturadocs.sql.ConsultasSQL;

@Repository
public interface ConsultarProcesosDao extends CrudRepository<TcomSolicitudCompra, Long>{
	 
//	@Transactional(readOnly = true)
//	@Query(value = "SELECT tcomSolicitudCompra "
//			+ "FROM TcomSolicitudCompra tcomSolicitudCompra "
//			+ "where tcomSolicitudCompra.codigo = :codigoProceso"
//			+ "order by tcomSolicitudCompra.idSoliCompra asc") 
//	List<TcomSolicitudCompra> findByCodigo(String codigoProceso);
	
	@Query(value = ConsultasSQL.CONSULTA_DATOS_POR_CODIGO_PROCESO, nativeQuery = true)
	List<Object[]> findByCodigo(String codigoProceso);
	
	

}
