package com.gob.ec.sercop.lecturadocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConsultarProcesoResponseDto {
	
	private Long idSoliCompra;
	
	private String codigoProceso;
	
	private String descripcionCompra;
	
	private Long codigoTipoProceso;
	
	private String tipoProceso;
	
	private String tipoDeRegimen;
	
	private String fechaPublicacion;
	
	private String valorAdjudicado;
	
	private String fechaAdjudicacion;
	
	private String proveedor;
	
	private String cedulaProveedor;	
	
	private String nombreArchivo;
	
	private String descripcionArchivo;
	
	private String tipoArchivo;
	
	private String seqTipoArchivo;
	
	private String urlArchivo;

}
