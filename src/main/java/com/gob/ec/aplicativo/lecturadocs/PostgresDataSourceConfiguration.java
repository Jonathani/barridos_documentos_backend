package com.gob.ec.sercop.lecturadocs;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.gob.ec.sercop.lecturadocs.daoPostgres",
entityManagerFactoryRef = "postgresEntityManagerFactory",
transactionManagerRef= "postgresTransactionManager")
public class PostgresDataSourceConfiguration {
	
	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource.lectura")
	public DataSourceProperties postgresDatasourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource.lectura.configuration")
	public DataSource postgresDataSource() {
		return postgresDatasourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	@Bean(name = "postgresEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean postgresEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(postgresDataSource()).packages("com.gob.ec.sercop.lecturadocs.model.postgres")
				.persistenceUnit("lectura").build();
	}

	@Bean(name = "postgresTransactionManager")
	public PlatformTransactionManager postgresTransactionManager(final @Qualifier("postgresEntityManagerFactory") LocalContainerEntityManagerFactoryBean postgresEntityManagerFactory) {
		return new JpaTransactionManager(postgresEntityManagerFactory.getObject());
	}

}
