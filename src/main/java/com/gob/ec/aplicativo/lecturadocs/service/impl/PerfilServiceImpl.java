package com.gob.ec.sercop.lecturadocs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.lecturadocs.daoPostgres.PerfilesDao;
import com.gob.ec.sercop.lecturadocs.dto.PerfilesResponseDto;
import com.gob.ec.sercop.lecturadocs.model.postgres.Perfil;
import com.gob.ec.sercop.lecturadocs.service.PerfilService;

@Service
public class PerfilServiceImpl implements PerfilService{

	@Autowired
	PerfilesDao perfilesDao;
	
	@Override
	public List<PerfilesResponseDto> listarPerfilesActivos() {		
		List<PerfilesResponseDto> listaPerfilesResponseDto = new ArrayList<>();
		List<Perfil> listaPerfiles = new ArrayList<>();		
		listaPerfiles = perfilesDao.findByEstado(true);		
		for (Perfil perfil: listaPerfiles) {
			listaPerfilesResponseDto.add(new PerfilesResponseDto(perfil.getIdPerfil(), perfil.getDescripcion().toUpperCase()));
		}
		return listaPerfilesResponseDto;
	}

	@Override
	public Perfil obtenerPerfilByIdPerfil(long idPerfil) {
		return perfilesDao.findByIdPerfil(idPerfil);
	}

}
