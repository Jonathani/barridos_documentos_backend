package com.gob.ec.sercop.lecturadocs.daoMongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.gob.ec.sercop.lecturadocs.model.mongodb.LecturaDocumentos;

public interface LecturaDocumentosDao extends MongoRepository<LecturaDocumentos, String>{
	
	@Query(value="{codigoProceso:'?0', palabraOriginal:'?1'}", fields="{'id': 1, 'codigoProceso': 1, 'descripcionArchivo': 1, 'idSoliCompra': 1, 'idUsuario': 1, 'nombreArchivo': 1, 'nombreUsuario': 1, 'palabraOriginal': 1, 'textoArchivo': 1, 'textoTotal': 1, 'fechaRegistro': 1, 'numeroPaginas': 1, 'urlPagina': 1, 'tipoArchivo': 1, 'descripcionCompra': 1, 'codigoTipoProceso': 1, 'tipoProceso': 1, 'tipoDeRegimen': 1, 'fechaPublicacion': 1, 'valorAdjudicado': 1, 'fechaAdjudicacion': 1, 'proveedor': 1, 'cedulaProveedor': 1, 'cudim': 1}")
	List<LecturaDocumentos> listarLecturaArchivoPorCodigo(String codigoProceso, String palabraOriginal);
	
	@Query(value="{}", fields="{'id': 1, 'codigoProceso': 1, 'descripcionArchivo': 1, 'idSoliCompra': 1, 'nombreArchivo': 1, 'palabraOriginal': 1, 'numeroPaginas': 1, 'urlPagina': 1, 'tipoArchivo': 1, 'precio': 1, 'nombreUsuarioActualizado': 1, 'fechaActualizacion': 1, 'fechaRegistro': 1, 'nombreUsuario': 1, 'descripcionCompra': 1, 'codigoTipoProceso': 1, 'tipoProceso': 1, 'tipoDeRegimen': 1, 'fechaPublicacion': 1, 'valorAdjudicado': 1, 'fechaAdjudicacion': 1, 'proveedor': 1, 'cedulaProveedor': 1, 'cudim': 1}")
	List<LecturaDocumentos> listarLecturaDocumentos();
	
	@Query(value="{id:'?0'}", fields="{'id': 1, 'codigoProceso': 1, 'descripcionArchivo': 1, 'idSoliCompra': 1, 'idUsuario': 1, 'nombreArchivo': 1, 'nombreUsuario': 1, 'palabraOriginal': 1, 'textoArchivo': 1, 'textoTotal': 1, 'fechaRegistro': 1, 'numeroPaginas': 1, 'urlPagina': 1, 'tipoArchivo': 1, 'descripcionCompra': 1, 'codigoTipoProceso': 1, 'tipoProceso': 1, 'tipoDeRegimen': 1, 'fechaPublicacion': 1, 'valorAdjudicado': 1, 'fechaAdjudicacion': 1, 'proveedor': 1, 'cedulaProveedor': 1, , 'cudim': 1}")
	LecturaDocumentos obtenerDocumentoPorId(String id);

}
