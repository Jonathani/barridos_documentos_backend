package com.gob.ec.sercop.lecturadocs.dto;

import lombok.Getter;
import lombok.Setter;

public class UsuariosRequestDto {
	
	@Getter
	@Setter
	private String identificacion;

	public UsuariosRequestDto() {
		super();
	}
	
	public UsuariosRequestDto(String identificacion) {
		super();
		this.identificacion = identificacion;
	}	

}
