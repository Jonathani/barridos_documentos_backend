package com.gob.ec.sercop.lecturadocs.dto;

import lombok.Getter;
import lombok.Setter;

public class BarridoDocumentosDto {
	
	@Getter
	@Setter
	private String codigo;

	@Getter
	@Setter
	private String buscar;

	@Getter
	@Setter
	private String nombreArchivo;

	@Getter
	@Setter
	private String texto;

	@Getter
	@Setter
	private String numeroPaginas;

	@Getter
	@Setter
	private String urlsPaginas;

	@Getter
	@Setter
	private String fechaRegistro;
	
	public BarridoDocumentosDto() {
		super();
	}

	public BarridoDocumentosDto(String codigo, String buscar, String nombreArchivo, String texto, String numeroPaginas,
			String urlsPaginas, String fechaRegistro) {
		super();
		this.codigo = codigo;
		this.buscar = buscar;
		this.nombreArchivo = nombreArchivo;
		this.texto = texto;
		this.numeroPaginas = numeroPaginas;
		this.urlsPaginas = urlsPaginas;
		this.fechaRegistro = fechaRegistro;
	}

}
