package com.gob.ec.sercop.lecturadocs.service;

import java.util.List;

import com.gob.ec.sercop.lecturadocs.dto.RegistrarConsultaRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.RegistrarConsultaResponseDto;

public interface RegistroConsultaService {
	
	String registrarConsulta(RegistrarConsultaRequestDto registrarConsultaRequestDto);
	
	List<RegistrarConsultaResponseDto> listarTodosLosRegistrosConsultas();

}
