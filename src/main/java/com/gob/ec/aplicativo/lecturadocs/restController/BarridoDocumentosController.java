package com.gob.ec.sercop.lecturadocs.restController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.lecturadocs.dto.BarridoDocsDetRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.BarridoDocsDetResponseDto;
import com.gob.ec.sercop.lecturadocs.dto.BarridoDocumentosCabDto;
import com.gob.ec.sercop.lecturadocs.dto.BarridoDocumentosDto;
import com.gob.ec.sercop.lecturadocs.service.BarridoDocumentosService;

@RestController
@RequestMapping("api/barridoDocumentos")
public class BarridoDocumentosController {

	@Autowired
	BarridoDocumentosService barridoDocumentosService;

	public static final String MENSAJE = "mensaje";

	@PostMapping(value = "listarDocumentos", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> documentosBarridosList() {
		Map<String, Object> responseMap = new HashMap<>();
		List<BarridoDocumentosDto> listaBarridoDocumentosDto = null;
		try {
			listaBarridoDocumentosDto = barridoDocumentosService.findAll();
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<List<BarridoDocumentosDto>>(listaBarridoDocumentosDto, HttpStatus.OK);
	}

	@PostMapping(value = "listarDocumentosCab", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> documentosBarridosCabList() {
		Map<String, Object> responseMap = new HashMap<>();
		List<BarridoDocumentosCabDto> listaBarridoDocumentosCabDto = null;
		try {
			listaBarridoDocumentosCabDto = barridoDocumentosService.listarBarridoDocsCabAll();
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<List<BarridoDocumentosCabDto>>(listaBarridoDocumentosCabDto, HttpStatus.OK);
	}

	@PostMapping(value = "listarDocumentosDet", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> documentosBarridosDetList(@RequestBody BarridoDocsDetRequestDto barridoDocsDetRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		List<BarridoDocsDetResponseDto> listaBarridoDocsDetResponseDto = null;
		try {
			listaBarridoDocsDetResponseDto = barridoDocumentosService
					.listarBarridoDocsDetallado(barridoDocsDetRequestDto.getCodigoProceso());
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<List<BarridoDocsDetResponseDto>>(listaBarridoDocsDetResponseDto, HttpStatus.OK);
	}
}
