package com.gob.ec.sercop.lecturadocs.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ListaConsultarProcesoResponseDto {
	
	private boolean existeCodigoProceso;
	
	private List<ConsultarProcesoResponseDto> consultarProcesoResponseDto;

}
