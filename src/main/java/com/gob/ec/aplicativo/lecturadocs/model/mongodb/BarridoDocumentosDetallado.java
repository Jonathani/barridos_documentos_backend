package com.gob.ec.sercop.lecturadocs.model.mongodb;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("barrido_documentos_detallado")
public class BarridoDocumentosDetallado {
	
	@Id
	private String id;
	
	@Field("codigo")
	private String codigo;
	
	@Field("buscar")
	private String buscar;
	
	@Field("nombre_archivo")
	private String nombreArchivo;
	
	@Field("texto")
	private String texto;
	
	@Field("num_paginas")
	private String numeroPaginas;
	
	@Field("urls_paginas")
	private String urlsPaginas;
	
	@Field("fecha_registro")
	private String fechaRegistro;	
	
	@Field("descripcion_archivo")
	private String descripcionArchivo;	
}
