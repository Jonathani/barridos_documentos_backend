package com.gob.ec.sercop.lecturadocs.daoMongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.gob.ec.sercop.lecturadocs.model.mongodb.BarridoDocumentosCab;

@Repository
public interface BarridosDocumentosCabDao extends MongoRepository<BarridoDocumentosCab, String>{	

	@Query(value="{}", fields="{'codigo': 1, 'descripcionCompra': 1, 'fechaPublicacion': 1, 'estado': 1}")
	List<BarridoDocumentosCab> listarBarridosDocsCabAll();
}
