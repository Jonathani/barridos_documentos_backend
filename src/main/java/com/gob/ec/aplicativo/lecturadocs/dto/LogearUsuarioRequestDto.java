package com.gob.ec.sercop.lecturadocs.dto;

import lombok.Getter;
import lombok.Setter;

public class LogearUsuarioRequestDto {
	
	@Getter
	@Setter
	private String identificacion;
	
	@Getter
	@Setter
	private String contrasenia;
	
	public LogearUsuarioRequestDto() {
		super();
	}

	public LogearUsuarioRequestDto(String identificacion, String contrasenia) {
		super();
		this.identificacion = identificacion;
		this.contrasenia = contrasenia;
	}	

}
