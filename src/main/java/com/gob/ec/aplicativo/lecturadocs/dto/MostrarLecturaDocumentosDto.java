package com.gob.ec.sercop.lecturadocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MostrarLecturaDocumentosDto {
	
	private String id;
	
	private String codigoProceso;
	
	private String descripcionArchivo;
	
	private String idSoliCompra;
	
	private String nombreArchivo;
	
	private String numeroPaginas;
	
	private String urlPagina;
	
	private String tipoArchivo;
	
	private String palabraBusqueda;
	
	private String precio;
	
	private String nombreUsuarioActualizado;
	
	private String fechaActualizacion;
	
	private String fechaRegistro;
	
	private String nombreUsuarioRegistro;
	
	private String descripcionCompra;
	
	private String codigoTipoProceso;
	
	private String tipoProceso;
	
	private String tipoDeRegimen;
	
	private String fechaPublicacion;
	
	private String valorAdjudicado;
	
	private String fechaAdjudicacion;
	
	private String proveedor;
	
	private String cedulaProveedor;
	
	private String cudim;
}
