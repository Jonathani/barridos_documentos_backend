package com.gob.ec.sercop.lecturadocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InfoSesionResponseDto {
	
	private Long idUsuario;
	
	private String identificacion;
	
	private String nombreCompleto;
	
	private Boolean estado;
	
	private Boolean estadoContrasenia;
	
	private String correo;
	
	private Long idPerfil;
	
	private String descripcionPerfil;

}
