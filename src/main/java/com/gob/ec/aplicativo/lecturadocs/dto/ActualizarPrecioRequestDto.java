package com.gob.ec.sercop.lecturadocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarPrecioRequestDto {
	
	private String id;
	
	private String precio;
	
	private String idUsuarioActualizado;
	
	private String nombreUsuarioActualizado;
	
	private String cudim;

}
