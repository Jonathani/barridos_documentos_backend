package com.gob.ec.sercop.lecturadocs;

import java.util.Collection;
import java.util.Collections;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Configuration
@EnableMongoRepositories(basePackages = "com.gob.ec.sercop.lecturadocs.daoMongodb")
public class MongodbDataSourceConfiguration {

	protected String getDatabaseName() {
		return "test";
	}

	public MongoClient mongoClient() {
		final ConnectionString connectionString = new ConnectionString("mongodb://administrador:administrador@172.27.27.54:27017/admin?retryWrites=true&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1");
		final MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
				.applyConnectionString(connectionString).build();
		return MongoClients.create(mongoClientSettings);
	}

	public Collection<String> getMappingBasePackages() {
		return Collections.singleton("org.spring.mongo.demo");
	}

//	@Bean
//	@Primary
//	@ConfigurationProperties("spring.data.mongodb")
//	public DataSourceProperties mongoDBDatasourceProperties() {
//		return new DataSourceProperties();
//	}
//
//	@Bean
//	@Primary
//	@ConfigurationProperties("spring.data.mongodb.configuration")
//	public DataSource mongoDBDataSource() {
//		return mongoDBDatasourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
//	}
//
//	@Bean(name = "mongoDBEntityManagerFactory")
//	public LocalContainerEntityManagerFactoryBean mongoDBEntityManagerFactory(EntityManagerFactoryBuilder builder) {
//		return builder.dataSource(mongoDBDataSource()).packages("com.gob.ec.sercop.lecturadocs.model.mongodb").build();
//	}
//
//	@Bean(name = "mongoDBTransactionManager")
//	public PlatformTransactionManager mongoDBTransactionManager(final @Qualifier("mongoDBEntityManagerFactory") LocalContainerEntityManagerFactoryBean mongoDBEntityManagerFactory) {
//		return new JpaTransactionManager(mongoDBEntityManagerFactory.getObject());
//	}

}
