package com.gob.ec.sercop.lecturadocs.model.mongodb;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("lectura_archivo")
public class LecturaDocumentos {
	
	@Id
	private String id;
	
	@Field("codigo_proceso")
	private String codigoProceso;
	
	@Field("descripcion_archivo")
	private String descripcionArchivo;
	
	@Field("id_soli_compra")
	private String idSoliCompra;
	
	@Field("id_usuario")
	private String idUsuario;
	
	@Field("nombre_archivo")
	private String nombreArchivo;
	
	@Field("nombre_usuario")
	private String nombreUsuario;
	
	@Field("palabra_original")
	private String palabraOriginal;
	
	@Field("texto_archivo")
	private String textoArchivo;
	
	@Field("texto_total")
	private String textoTotal;

	@Field("fecha_registro")
	private String fechaRegistro;
	
	@Field("numero_paginas")
	private String numeroPaginas;
	
	@Field("url_pagina")
	private String urlPagina;
	
	@Field("tipo_archivo")
	private String tipoArchivo;
	
	@Field("precio")
	private String precio;
	
	@Field("id_usuario_actualizado")
	private String idUsuarioActualizado;
	
	@Field("nombre_usuario_actualizado")
	private String nombreUsuarioActualizado;
	
	@Field("fecha_actualizacion")
	private String fechaActualizacion;
	
	@Field("descripcion_compra")
	private String descripcionCompra;
	
	@Field("codigo_tipo_proceso")
	private String codigoTipoProceso;
	
	@Field("tipo_proceso")
	private String tipoProceso;
	
	@Field("tipo_de_regimen")
	private String tipoDeRegimen;
	
	@Field("fecha_publicacion")
	private String fechaPublicacion;
	
	@Field("valor_adjudicado")
	private String valorAdjudicado;
	
	@Field("fecha_adjudicacion")
	private String fechaAdjudicacion;
	
	@Field("proveedor")
	private String proveedor;
	
	@Field("cedula_proveedor")
	private String cedulaProveedor;
	
	@Field("cudim")
	private String cudim;
}
