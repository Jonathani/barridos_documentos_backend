package com.gob.ec.sercop.lecturadocs.dto;

import lombok.Getter;
import lombok.Setter;

public class BarridoDocsDetResponseDto {
	
	@Getter
	@Setter
	private String codigo;

	@Getter
	@Setter
	private String buscar;

	@Getter
	@Setter
	private String nombreArchivo;

	@Getter
	@Setter
	private String texto;

	@Getter
	@Setter
	private String numeroPaginas;

	@Getter
	@Setter
	private String urlsPaginas;

	@Getter
	@Setter
	private String fechaRegistro;	
	
	@Getter
	@Setter
	private String descripcionArchivo;

	public BarridoDocsDetResponseDto() {
		super();
	}

	public BarridoDocsDetResponseDto(String codigo, String buscar, String nombreArchivo, String texto,
			String numeroPaginas, String urlsPaginas, String fechaRegistro, String descripcionArchivo) {
		super();
		this.codigo = codigo;
		this.buscar = buscar;
		this.nombreArchivo = nombreArchivo;
		this.texto = texto;
		this.numeroPaginas = numeroPaginas;
		this.urlsPaginas = urlsPaginas;
		this.fechaRegistro = fechaRegistro;
		this.descripcionArchivo = descripcionArchivo;
	}
	
}
