package com.gob.ec.sercop.lecturadocs.model.mongodb;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Getter;
import lombok.Setter;

@Document("barrido_documentos_cab")
public class BarridoDocumentosCab {
	
	@Id
	private String id;
	
	@Getter
	@Setter
	@Field("codigo")
	private String codigo;
	
	@Getter
	@Setter
	@Field("desc_compra")
	private String descripcionCompra;
	
	@Getter
	@Setter
	@Field("fecha_publicacion")
	private String fechaPublicacion;
	
	@Getter
	@Setter
	@Field("estado")
	private String estado;	

	public BarridoDocumentosCab() {
		super();
	}
	
	public BarridoDocumentosCab(String id, String codigo, String descripcionCompra, String fechaPublicacion,
			String estado) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.descripcionCompra = descripcionCompra;
		this.fechaPublicacion = fechaPublicacion;
		this.estado = estado;
	}	

}
