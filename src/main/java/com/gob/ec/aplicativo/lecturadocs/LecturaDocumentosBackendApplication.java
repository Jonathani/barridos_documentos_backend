package com.gob.ec.sercop.lecturadocs;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LecturaDocumentosBackendApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(LecturaDocumentosBackendApplication.class);
        app.setDefaultProperties(Collections.singletonMap("server.port", "8082"));
        app.run(args);
	}

}
