package com.gob.ec.sercop.lecturadocs.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegistrarConsultaRequestDto {
	
	private Integer idUsuario;
	
	private String procedimientos;
	
	private String caracteristicas;
	
	//private MultipartFile archivo;

}
