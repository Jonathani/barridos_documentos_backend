package com.gob.ec.sercop.lecturadocs.model.postgresmic;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the tcom_archivo database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tcom_archivo")
@NamedQuery(name="TcomArchivo.findAll", query="SELECT t FROM TcomArchivo t")
public class TcomArchivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_archivo")
	private Integer idArchivo;
	
	@Column(name="id_soli_compra")
	private Integer idSoliCompra; 
	
	@Column(name="url_archivo")
	private String urlArchivo;
	
	@Column(name="desc_archivo")
	private String descArchivo;
	
	@Column(name="seq_tipo_archivo")
	private Integer seqTipoArchivo;
	

}