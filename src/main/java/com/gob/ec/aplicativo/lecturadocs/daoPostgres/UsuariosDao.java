package com.gob.ec.sercop.lecturadocs.daoPostgres;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.lecturadocs.model.postgres.Usuario;

@Repository
public interface UsuariosDao extends CrudRepository<Usuario, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT usuario "
			+ "FROM Usuario usuario "
			+ "order by usuario.idUsuario asc")
	public List<Usuario> findAll();
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT usuario "
			+ "FROM Usuario usuario "
			+ "WHERE usuario.identificacion = :identificacion order by usuario.idUsuario asc")
	public List<Usuario> findByIdentificacion(String identificacion);
	
	@Transactional(readOnly = true)
	@Query(value = Usuario.OBTENER_USUARIO_POR_IDENTIFICACION)
	public Usuario obtenerUsuarioPorIdentificacion(String identificacion);
		
	@Transactional(readOnly = true)
	@Query(value = "SELECT usuario "
			+ "FROM Usuario usuario "
			+ "WHERE usuario.idUsuario= :idUsuario")
	public Usuario findByIdUsuario(long idUsuario);
	
	@Transactional(readOnly = true)
	@Query(value = Usuario.OBTENER_INFORMACION_DE_USUARIO_POR_IDENTIFICACION)
	public List<Object[]> obtenerInformacionDeUsuarioPorIdentificacion(String identificacion);
	
	@Transactional(readOnly = true)
	@Query(Usuario.OBTENER_INFORMACION_RECUPERAR_CORREO)
	public Usuario obtenerInformacionRecuperarCorreo(String identificacion, String correo);
	
	@Transactional(readOnly = true)
	@Query(Usuario.OBTENER_INFORMACION_DE_TODOS_LOS_USUARIOS)
	public List<Object[]> obtenerInformacionDeTodosLosUsuarios();

}
