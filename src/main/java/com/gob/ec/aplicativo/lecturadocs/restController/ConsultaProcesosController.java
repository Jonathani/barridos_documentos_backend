package com.gob.ec.sercop.lecturadocs.restController;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.lecturadocs.dto.ActualizarPrecioRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.ConsultarProcesoRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.ConsultarProcesoResponseDto;
import com.gob.ec.sercop.lecturadocs.dto.ListaConsultarProcesoResponseDto;
import com.gob.ec.sercop.lecturadocs.dto.MostrarLecturaDocumentosDto;
import com.gob.ec.sercop.lecturadocs.dto.RegistrarConsultaRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.RegistrarConsultaResponseDto;
import com.gob.ec.sercop.lecturadocs.service.ConsultarProcesosService;
import com.gob.ec.sercop.lecturadocs.service.LecturaDocumentosService;
import com.gob.ec.sercop.lecturadocs.service.RegistroConsultaService;
import com.gob.ec.sercop.utilitarioGeneral.properties.UtilitarioProperties;

@RestController
@RequestMapping("api/consultaProcesos")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ConsultaProcesosController {
	
	@Autowired
	ConsultarProcesosService consultarProcesosService;
	
	@Autowired
	LecturaDocumentosService lecturaDocumentosService;
	
	@Autowired
	RegistroConsultaService registroConsultaService;
	
	public static final String MENSAJE = "mensaje";
	
	public static final String UPLOADER_FOLDER = "/home/user/Documentos/NuevoSercop/ProyectosSercop/lectura_documentos_frontend/public/archivos_justificacion/";
	
	@PostMapping(value = "codigoDeProceso", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> documentosBarridosList(@RequestBody ConsultarProcesoRequestDto consultarProcesoRequestDto) {
		
		Map<String, Object> responseMap = new HashMap<>();
		List<ConsultarProcesoResponseDto> listaConsultarProcesoResponseDto = new ArrayList<>();
		ListaConsultarProcesoResponseDto consultarProcesoResponseDto;
		try {
			boolean existeCodigoProceso =lecturaDocumentosService.existeCodigoProceso(consultarProcesoRequestDto);
			if (!existeCodigoProceso) {
				listaConsultarProcesoResponseDto = consultarProcesosService.listarConsultaProcesos(consultarProcesoRequestDto);								
			}
			consultarProcesoResponseDto = new ListaConsultarProcesoResponseDto(existeCodigoProceso, listaConsultarProcesoResponseDto);
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ListaConsultarProcesoResponseDto>(consultarProcesoResponseDto, HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@PostMapping(value = "listarTodosLosDocumentos", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> listarTodosLosDocumentos() {
		
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, List> respuestaTotal = new HashMap<>();
		List<MostrarLecturaDocumentosDto> listaMostrarLecturaDocumentosDto = new ArrayList<>();
		try {
			listaMostrarLecturaDocumentosDto = lecturaDocumentosService.consultarTodosLosProcesos();
			respuestaTotal.put(MENSAJE, listaMostrarLecturaDocumentosDto);
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, List>>(respuestaTotal, HttpStatus.OK);
	}
	
	@PostMapping(value = "actualizarPrecioDocumento", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> actualizarPrecioDocumento(@RequestBody ActualizarPrecioRequestDto actualizarPrecioRequestDto) {
		
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, String> respuestaTotal = new HashMap<>();
		Boolean actualizado;
		try {
			actualizado = lecturaDocumentosService.actualizarPrecioPorCodigoProceso(actualizarPrecioRequestDto);
			if (actualizado) {
				respuestaTotal.put("status", actualizado.toString());
				respuestaTotal.put(MENSAJE, UtilitarioProperties.MENSAJE_ACTUALIZADO_PRECIO_OK);
			}else {
				respuestaTotal.put("status", actualizado.toString());
				respuestaTotal.put(MENSAJE, UtilitarioProperties.MENSAJE_ACTUALIZADO_PRECIO_ERROR);
			}
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, String>>(respuestaTotal, HttpStatus.OK);
	}
	
	@PostMapping(value = "registrarConsulta", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> registrarConsulta(@ModelAttribute RegistrarConsultaRequestDto registrarConsultaRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, String> respuestaTotal = new HashMap<>();
		
		LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");  
        String fechaActual = now.format(format);		
		Path uploadPath = Paths.get(UPLOADER_FOLDER);
		String registroConsultaId = null;
		
		try {
			
//			String nombreArchivo = registrarConsultaRequestDto.getArchivo().getName() + fechaActual + ".pdf";
//			String rutaArchivo = UPLOADER_FOLDER + nombreArchivo;  
//			Path filePath = uploadPath.resolve(nombreArchivo);
//			Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
			registroConsultaId = registroConsultaService.registrarConsulta(registrarConsultaRequestDto);			
			respuestaTotal.put("status", "true");
			respuestaTotal.put("mensaje", registroConsultaId);
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, String>>(respuestaTotal, HttpStatus.OK);
	}
	
	@PostMapping(value = "listarTodosRegConsultas", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> listarTodosRegConsultas() {
		
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, List> respuestaTotal = new HashMap<>();
		List<RegistrarConsultaResponseDto> listadoDeRegistros = new ArrayList<>();
		try {			
			listadoDeRegistros = registroConsultaService.listarTodosLosRegistrosConsultas();			
			respuestaTotal.put(MENSAJE, listadoDeRegistros);
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, List>>(respuestaTotal, HttpStatus.OK);
	}
}
