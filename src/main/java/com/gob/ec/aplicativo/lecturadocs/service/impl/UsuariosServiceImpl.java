package com.gob.ec.sercop.lecturadocs.service.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.lecturadocs.daoPostgres.PerfilUsuarioDao;
import com.gob.ec.sercop.lecturadocs.daoPostgres.PerfilesDao;
import com.gob.ec.sercop.lecturadocs.daoPostgres.UsuariosDao;
import com.gob.ec.sercop.lecturadocs.dto.ActualizarContraseniaRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.InfoSesionRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.InfoSesionResponseDto;
import com.gob.ec.sercop.lecturadocs.dto.LogearUsuarioRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.RecuperarContraseniaRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.RegistroUsuarioRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.UsuarioActualizarRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.UsuariosResponseDto;
import com.gob.ec.sercop.lecturadocs.model.postgres.Perfil;
import com.gob.ec.sercop.lecturadocs.model.postgres.PerfilUsuario;
import com.gob.ec.sercop.lecturadocs.model.postgres.Usuario;
import com.gob.ec.sercop.lecturadocs.service.UsuariosService;
import com.gob.ec.sercop.utilitarioGeneral.codificacion.AES256;
import com.gob.ec.sercop.utilitarioGeneral.codificacion.UtilitarioCadena;
import com.gob.ec.sercop.utilitarioGeneral.enviarCorreos.EnviarCorreos;
import com.gob.ec.sercop.utilitarioGeneral.generarClave.GenerarClave;
import com.gob.ec.sercop.utilitarioGeneral.properties.UtilitarioProperties;

@Service
public class UsuariosServiceImpl implements UsuariosService {

	@Autowired
	UsuariosDao usuariosDao;

	@Autowired
	PerfilesDao perfilesDao;

	@Autowired
	PerfilUsuarioDao perfilUsuarioDao;

	@Override
	public List<UsuariosResponseDto> listarInfoUsuario(String identificacion) {

		List<UsuariosResponseDto> listarInformacionUsuariosResp = new ArrayList<>();

		List<Usuario> listarInformacionUsuario = new ArrayList<>();		
		listarInformacionUsuario = usuariosDao.findByIdentificacion(identificacion);
		for (Usuario usuario : listarInformacionUsuario) {
			listarInformacionUsuariosResp.add(new UsuariosResponseDto(usuario.getIdUsuario(),
					usuario.getIdentificacion(), usuario.getNombreCompleto(), usuario.getFechaCreacion().toString(),
					usuario.getEstado().toString(), usuario.getCorreo()));
		}
		return listarInformacionUsuariosResp;
	}

	@Override
	public Boolean registrarUsuario(RegistroUsuarioRequestDto registroUsuarioRequestDto) {

		Boolean enviadoClave = false;
		String claveAleatoria = "";
		LocalDateTime now = LocalDateTime.now();
		Timestamp fechaActual = Timestamp.valueOf(now);
		Perfil perfil = null;

		if (usuariosDao.obtenerUsuarioPorIdentificacion(registroUsuarioRequestDto.getIdentificacion().trim()) == null) {
			// Persistir sobre la base de datos

			// Crear el nuevo usuario
			Usuario usuario = new Usuario();
			usuario.setIdentificacion(registroUsuarioRequestDto.getIdentificacion().trim());
			usuario.setNombreCompleto(registroUsuarioRequestDto.getNombresCompletos().trim());
			usuario.setCorreo(registroUsuarioRequestDto.getCorreo().trim());
			claveAleatoria = GenerarClave.generarClaveAleatoria(15L);
			usuario.setContrasenia(AES256.encrypt(claveAleatoria));
			usuario.setEstado(true);
			usuario.setEstadoContrasenia(false);
			usuario.setFechaCreacion(fechaActual);
			usuariosDao.save(usuario);

			// Crear el nuevo Perfil Usuario
			perfil = perfilesDao.findByIdPerfil(registroUsuarioRequestDto.getIdPerfil());
			PerfilUsuario perfilUsuario = new PerfilUsuario();
			perfilUsuario.setPerfil(perfil);
			perfilUsuario.setUsuario(usuario);
			perfilUsuario.setFechaAsignacion(fechaActual);
			perfilUsuarioDao.save(perfilUsuario);

			enviadoClave = true;

			// Enviar correo de confirmación
			StringBuffer correoHtmlElectronico = new StringBuffer();
			correoHtmlElectronico.append("<html>");
			correoHtmlElectronico.append("Estimado/a, <b>");
			correoHtmlElectronico.append(registroUsuarioRequestDto.getNombresCompletos().trim());
			correoHtmlElectronico.append("</b>");
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("En relación a la creación de su usuario, ");
			correoHtmlElectronico.append(registroUsuarioRequestDto.getNombresCompletos().trim());
			correoHtmlElectronico.append(", se ha procedido con la creación del usuario: <b> ");
			correoHtmlElectronico.append(registroUsuarioRequestDto.getIdentificacion().trim());
			correoHtmlElectronico.append("</b> y la contraseña  <b>");
			correoHtmlElectronico.append(claveAleatoria);
			correoHtmlElectronico.append("</b>.");
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("Por favor ingresar en la siguiente link: ");
			correoHtmlElectronico.append(UtilitarioProperties.URL_SISTEMA_LECTURA_DOCUMENTOS);
			correoHtmlElectronico.append(" y cambiar su contraseña.");
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("Atentamente,");
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("Servicio Nacional de Contratación Pública<br>");
			correoHtmlElectronico.append("Plataforma Gubernamental Financiera,Amazonas entre Unión Nacional de Periodistas y Alonso Pereira<br>");
			correoHtmlElectronico.append("<br>www.sercop.gob.ec<br>");
			correoHtmlElectronico.append("</html>");
			EnviarCorreos.sendEmail(registroUsuarioRequestDto.getCorreo(), "Creación de Usuario",UtilitarioCadena.reemplazarCaracteresEspecialesHtml(correoHtmlElectronico.toString()));

		}

		return enviadoClave;
	}

	@Override
	public Boolean logearUsuario(LogearUsuarioRequestDto logearUsuarioRequestDto) {
		Boolean usuarioLogeado = false;
		Usuario infoUsuario;
		infoUsuario = usuariosDao.obtenerUsuarioPorIdentificacion(logearUsuarioRequestDto.getIdentificacion());
		usuarioLogeado = infoUsuario != null
				? AES256.decrypt(infoUsuario.getContrasenia()).equals(logearUsuarioRequestDto.getContrasenia()) ? true
						: false
				: false;

		return usuarioLogeado;
	}

	@Override
	public List<UsuariosResponseDto> listarTodosLosUsuario() {
		List<UsuariosResponseDto> listarInformacionUsuariosResp = new ArrayList<>();
		List<Object[]> listarInformacionUsuario = usuariosDao.obtenerInformacionDeTodosLosUsuarios();
		for (Object[] infoUsuario: listarInformacionUsuario) {
			listarInformacionUsuariosResp.add(new UsuariosResponseDto(
				infoUsuario[0] != null ? Long.parseLong(infoUsuario[0].toString()): 0L,
				infoUsuario[1] != null ? infoUsuario[1].toString(): "",
				infoUsuario[2] != null ? infoUsuario[2].toString(): "",
				infoUsuario[3] != null ? infoUsuario[8].toString(): "",
				infoUsuario[4] != null ? infoUsuario[3].toString(): "",
				infoUsuario[5] != null ? infoUsuario[5].toString(): "",
				infoUsuario[6] != null ? Long.parseLong(infoUsuario[6].toString()): 0L,
				infoUsuario[7] != null ? infoUsuario[7].toString(): "",
				infoUsuario[10] != null ? Long.parseLong(infoUsuario[10].toString()): 0L
			));
		}
		return listarInformacionUsuariosResp;
	}

	@Override
	public void actualizarUsuario(UsuarioActualizarRequestDto usuarioActualizarRequestDto) {
		LocalDateTime now = LocalDateTime.now();
		Timestamp fechaActual = Timestamp.valueOf(now);
			
		//Actualizar información de usuario
		Usuario usuario = usuariosDao.findByIdUsuario(usuarioActualizarRequestDto.getIdUsuario());
		usuario.setCorreo(usuarioActualizarRequestDto.getCorreo());
		usuario.setEstado(usuarioActualizarRequestDto.getEstadoUsuario());
		usuario.setNombreCompleto(usuarioActualizarRequestDto.getNombresCompletos());
		usuario.setFechaActualizacion(fechaActual);
		usuariosDao.save(usuario);
		
		//Actualizar informacion de perfil usuario
		Perfil perfil = perfilesDao.findByIdPerfil(usuarioActualizarRequestDto.getIdPerfil());
		PerfilUsuario perfilUsuario = perfilUsuarioDao.findByIdPerfilUsuario(usuarioActualizarRequestDto.getIdPerfilUsuario());
		perfilUsuario.setPerfil(perfil);
		perfilUsuarioDao.save(perfilUsuario);
		
	}

	@Override
	public String actualizarContrasenia(ActualizarContraseniaRequestDto actualizarContraseniaRequestDto) {
		LocalDateTime now = LocalDateTime.now();
		Timestamp fechaActual = Timestamp.valueOf(now);
		String respuesta = "";
		Usuario usuario = usuariosDao.findByIdUsuario(actualizarContraseniaRequestDto.getIdUsuario());
		if (AES256.decrypt(usuario.getContrasenia()).equals(actualizarContraseniaRequestDto.getContraseniaActual())) {
			usuario.setContrasenia(AES256.encrypt(actualizarContraseniaRequestDto.getContraseniaNueva()));
			usuario.setEstadoContrasenia(true);
			usuario.setFechaActualizacion(fechaActual);
			usuariosDao.save(usuario);
			
			//Enviar recuperacion de contraseña
			StringBuffer correoHtmlElectronico = new StringBuffer();
			correoHtmlElectronico.append("<html>");
			correoHtmlElectronico.append("Estimado/a, <b>");
			correoHtmlElectronico.append(usuario.getNombreCompleto());
			correoHtmlElectronico.append("</b>");
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("Se ha realizado el cambio de la contraseña con éxito ");			
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("Por favor ingresar en la siguiente link: ");
			correoHtmlElectronico.append(UtilitarioProperties.URL_SISTEMA_LECTURA_DOCUMENTOS);
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("Atentamente,");
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("Servicio Nacional de Contratación Pública<br>");
			correoHtmlElectronico.append("Plataforma Gubernamental Financiera,Amazonas entre Unión Nacional de Periodistas y Alonso Pereira<br>");
			correoHtmlElectronico.append("<br>www.sercop.gob.ec<br>");
			correoHtmlElectronico.append("</html>");
			EnviarCorreos.sendEmail(usuario.getCorreo(), "Actualización de la Contraseña",UtilitarioCadena.reemplazarCaracteresEspecialesHtml(correoHtmlElectronico.toString()));
			
			respuesta = UtilitarioProperties.CONTRASENIA_ACTUALIZADA_OK;
		} else {
			respuesta = UtilitarioProperties.CONTRASENIA_ACTUALIZADA_ERROR;
		}
		return respuesta;
	}

	@Override
	public List<InfoSesionResponseDto> listarInformacionSesionUsuario(InfoSesionRequestDto infoSesionRequestDto) {
		List<InfoSesionResponseDto> listaInfoSesionResponseDto = new ArrayList<>();
		List<Object[]> listarInfoSesionUsuario = usuariosDao.obtenerInformacionDeUsuarioPorIdentificacion(infoSesionRequestDto.getIdentificacion());
		for (Object[] infoSesionUsuario : listarInfoSesionUsuario) {
			listaInfoSesionResponseDto.add(new InfoSesionResponseDto(Long.parseLong(infoSesionUsuario[0].toString()),
					infoSesionUsuario[1].toString(), infoSesionUsuario[2].toString(),
					Boolean.parseBoolean(infoSesionUsuario[3].toString()),
					Boolean.parseBoolean(infoSesionUsuario[4].toString()), infoSesionUsuario[5].toString(),
					Long.parseLong(infoSesionUsuario[6].toString()), infoSesionUsuario[7].toString()));
		}
		return listaInfoSesionResponseDto;
	}

	@Override
	public Boolean recuperarContrasenia(RecuperarContraseniaRequestDto recuperarContraseniaRequestDto) {
		
		Boolean respuesta = false;
		String claveAleatoria = "";
		LocalDateTime now = LocalDateTime.now();
		Timestamp fechaActual = Timestamp.valueOf(now);
		Usuario usuario = usuariosDao.obtenerInformacionRecuperarCorreo(recuperarContraseniaRequestDto.getIdentificacion().trim(), recuperarContraseniaRequestDto.getCorreo().trim());
		if (usuario != null) {
			claveAleatoria = GenerarClave.generarClaveAleatoria(15L);
			usuario.setContrasenia(AES256.encrypt(claveAleatoria));
			usuario.setEstadoContrasenia(false);
			usuario.setFechaActualizacion(fechaActual);
			usuariosDao.save(usuario);
			
			//Enviar recuperacion de contraseña
			StringBuffer correoHtmlElectronico = new StringBuffer();
			correoHtmlElectronico.append("<html>");
			correoHtmlElectronico.append("Estimado/a, <b>");
			correoHtmlElectronico.append(usuario.getNombreCompleto());
			correoHtmlElectronico.append("</b>");
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("En relación a la recuperación de la contraseña de su usuario, ");
			correoHtmlElectronico.append(usuario.getNombreCompleto());
			correoHtmlElectronico.append(", es la siguiente:<br> ");
			correoHtmlElectronico.append("Usuario: <b>");
			correoHtmlElectronico.append(usuario.getIdentificacion());
			correoHtmlElectronico.append("</b> y <br>");
			correoHtmlElectronico.append("La Contraseña: <b> ");
			correoHtmlElectronico.append(claveAleatoria);
			correoHtmlElectronico.append("</b> <br><br>");
			correoHtmlElectronico.append("Por favor ingresar en la siguiente link: ");
			correoHtmlElectronico.append(UtilitarioProperties.URL_SISTEMA_LECTURA_DOCUMENTOS);
			correoHtmlElectronico.append(" y cambiar su contraseña.");
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("Atentamente,");
			correoHtmlElectronico.append("<br><br>");
			correoHtmlElectronico.append("Servicio Nacional de Contratación Pública<br>");
			correoHtmlElectronico.append("Plataforma Gubernamental Financiera,Amazonas entre Unión Nacional de Periodistas y Alonso Pereira<br>");
			correoHtmlElectronico.append("<br>www.sercop.gob.ec<br>");
			correoHtmlElectronico.append("</html>");
			EnviarCorreos.sendEmail(usuario.getCorreo(), "Recuperación de Contraseña",UtilitarioCadena.reemplazarCaracteresEspecialesHtml(correoHtmlElectronico.toString()));
			respuesta = true;
		}
		return respuesta;
	}
}
