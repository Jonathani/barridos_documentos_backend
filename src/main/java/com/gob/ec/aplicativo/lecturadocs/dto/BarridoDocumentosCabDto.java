package com.gob.ec.sercop.lecturadocs.dto;

import lombok.Getter;
import lombok.Setter;

public class BarridoDocumentosCabDto {
	
	@Setter
	@Getter
	private String diccionarioDeDatos;
	
	@Setter
	@Getter
	private String codigoProceso;
	
	@Setter
	@Getter
	private String descripcionProceso;
	
	@Setter
	@Getter
	private String fechaPublicacion;
	
	@Setter
	@Getter
	private String estadoActual;	

	public BarridoDocumentosCabDto() {
		super();
	}

	public BarridoDocumentosCabDto(String diccionarioDeDatos, String codigoProceso, String descripcionProceso,
			String fechaPublicacion, String estadoActual) {
		super();
		this.diccionarioDeDatos = diccionarioDeDatos;
		this.codigoProceso = codigoProceso;
		this.descripcionProceso = descripcionProceso;
		this.fechaPublicacion = fechaPublicacion;
		this.estadoActual = estadoActual;
	}
	
}
