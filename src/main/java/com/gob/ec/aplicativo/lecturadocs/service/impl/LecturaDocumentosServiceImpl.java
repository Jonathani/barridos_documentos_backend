package com.gob.ec.sercop.lecturadocs.service.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.lecturadocs.daoMongodb.LecturaDocumentosDao;
import com.gob.ec.sercop.lecturadocs.dto.ActualizarPrecioRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.ConsultarProcesoRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.MostrarLecturaDocumentosDto;
import com.gob.ec.sercop.lecturadocs.model.mongodb.LecturaDocumentos;
import com.gob.ec.sercop.lecturadocs.service.LecturaDocumentosService;

@Service
public class LecturaDocumentosServiceImpl implements LecturaDocumentosService{
	
	@Autowired
	LecturaDocumentosDao lecturaDocumentosDao;

	@Override
	public Boolean existeCodigoProceso(ConsultarProcesoRequestDto consultarProcesoRequestDto) {
		
		boolean existeElCodigoProceso = false;
		List<LecturaDocumentos> listaLecturaDocumentos = null;
		listaLecturaDocumentos = lecturaDocumentosDao.listarLecturaArchivoPorCodigo(consultarProcesoRequestDto.getCodigoProceso().trim(), consultarProcesoRequestDto.getPalabrasBusqueda().trim().toLowerCase());
		if (listaLecturaDocumentos.size() > 0) {
			existeElCodigoProceso = true;
		}		
		return existeElCodigoProceso;
	}

	@Override
	public List<MostrarLecturaDocumentosDto> consultarTodosLosProcesos() {
		
		List<MostrarLecturaDocumentosDto> listaMostrarLecturaDocumentosDto = new ArrayList<>();
		List<LecturaDocumentos> listaLecturaDocumentos = lecturaDocumentosDao.listarLecturaDocumentos();		
		for (LecturaDocumentos lecturaDocumentos: listaLecturaDocumentos) {
			listaMostrarLecturaDocumentosDto.add(new MostrarLecturaDocumentosDto(
					lecturaDocumentos.getId(), 
					lecturaDocumentos.getCodigoProceso(), 
					lecturaDocumentos.getDescripcionArchivo(), 
					lecturaDocumentos.getIdSoliCompra(),
					lecturaDocumentos.getNombreArchivo(), 
					lecturaDocumentos.getNumeroPaginas(), 
					lecturaDocumentos.getUrlPagina(), 
					lecturaDocumentos.getTipoArchivo(),
					lecturaDocumentos.getPalabraOriginal(), 
					lecturaDocumentos.getPrecio(),
					lecturaDocumentos.getNombreUsuarioActualizado(),
					lecturaDocumentos.getFechaActualizacion(),
					lecturaDocumentos.getFechaRegistro(),
					lecturaDocumentos.getNombreUsuario(),
					lecturaDocumentos.getDescripcionCompra(),
					lecturaDocumentos.getCodigoTipoProceso(),
					lecturaDocumentos.getTipoProceso(),
					lecturaDocumentos.getTipoDeRegimen(),
					lecturaDocumentos.getFechaPublicacion(),
					lecturaDocumentos.getValorAdjudicado(),
					lecturaDocumentos.getFechaAdjudicacion(),
					lecturaDocumentos.getProveedor(),
					lecturaDocumentos.getCedulaProveedor(),
					lecturaDocumentos.getCudim())
					
			);
		}
		return listaMostrarLecturaDocumentosDto;
	}

	@Override
	public Boolean actualizarPrecioPorCodigoProceso(ActualizarPrecioRequestDto actualizarPrecioRequestDto) {
		boolean actualizado = false;
		LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
        String fechaActual = now.format(format);
		LecturaDocumentos lecturaDocumentos= lecturaDocumentosDao.obtenerDocumentoPorId(actualizarPrecioRequestDto.getId());
		if (lecturaDocumentos != null) {
			lecturaDocumentos.setPrecio(actualizarPrecioRequestDto.getPrecio());
			lecturaDocumentos.setIdUsuarioActualizado(actualizarPrecioRequestDto.getIdUsuarioActualizado());
			lecturaDocumentos.setNombreUsuarioActualizado(actualizarPrecioRequestDto.getNombreUsuarioActualizado());
			lecturaDocumentos.setCudim(actualizarPrecioRequestDto.getCudim());
			lecturaDocumentos.setFechaActualizacion(fechaActual.toString());
			lecturaDocumentosDao.save(lecturaDocumentos);
			actualizado = true;			
		}
		return actualizado;		
	}

}
