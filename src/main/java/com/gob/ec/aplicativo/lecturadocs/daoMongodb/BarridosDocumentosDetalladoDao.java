package com.gob.ec.sercop.lecturadocs.daoMongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.gob.ec.sercop.lecturadocs.model.mongodb.BarridoDocumentosDetallado;

@Repository
public interface BarridosDocumentosDetalladoDao extends MongoRepository<BarridoDocumentosDetallado, String>{
	
	@Query(value="{}", fields="{'id': 1, 'codigo': 1, 'buscar': 1, 'nombreArchivo': 1, 'texto': 1, 'numeroPaginas': 1, 'urlsPaginas': 1, 'fechaRegistro': 1}")
	List<BarridoDocumentosDetallado> findAll();
	
	@Query(value="{codigo:'?0'}", fields="{'id': 1, 'codigo': 1, 'buscar': 1, 'nombreArchivo': 1, 'texto': 1, 'numeroPaginas': 1, 'urlsPaginas': 1, 'fechaRegistro': 1, 'descripcionArchivo': 1}")
	List<BarridoDocumentosDetallado> listarBarridosDocsDetallado(String codigo);
}
