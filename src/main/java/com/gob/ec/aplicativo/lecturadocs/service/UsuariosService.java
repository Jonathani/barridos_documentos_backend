package com.gob.ec.sercop.lecturadocs.service;

import java.util.List;

import com.gob.ec.sercop.lecturadocs.dto.ActualizarContraseniaRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.InfoSesionRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.InfoSesionResponseDto;
import com.gob.ec.sercop.lecturadocs.dto.LogearUsuarioRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.RecuperarContraseniaRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.RegistroUsuarioRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.UsuarioActualizarRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.UsuariosResponseDto;

public interface UsuariosService {
	
	List<UsuariosResponseDto> listarInfoUsuario(String identificacion);
	
	List<UsuariosResponseDto> listarTodosLosUsuario();
	
	Boolean registrarUsuario(RegistroUsuarioRequestDto registroUsuarioRequestDto);
	
	Boolean logearUsuario(LogearUsuarioRequestDto logearUsuarioRequestDto);
	
	void actualizarUsuario(UsuarioActualizarRequestDto usuarioActualizarRequestDto);
	
	String actualizarContrasenia(ActualizarContraseniaRequestDto actualizarContraseniaRequestDto);
	
	List<InfoSesionResponseDto> listarInformacionSesionUsuario(InfoSesionRequestDto infoSesionRequestDto);
	
	Boolean recuperarContrasenia(RecuperarContraseniaRequestDto recuperarContraseniaRequestDto);

}
