package com.gob.ec.sercop.lecturadocs.restController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.lecturadocs.dto.ActualizarContraseniaRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.InfoSesionRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.LogearUsuarioRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.RecuperarContraseniaRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.RegistroUsuarioRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.RespuestaInformacionSesion;
import com.gob.ec.sercop.lecturadocs.dto.UsuarioActualizarRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.UsuariosRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.UsuariosResponseDto;
import com.gob.ec.sercop.lecturadocs.service.UsuariosService;
import com.gob.ec.sercop.utilitarioGeneral.properties.UtilitarioProperties;

@RestController
@RequestMapping("api/usuariosController")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class UsuariosController {
	
	@Autowired
	UsuariosService usuariosService;
	
	public static final String MENSAJE = "mensaje";
	
	@PostMapping(value = "listarInfoUsuario", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> listarInfoUsuario(@RequestBody UsuariosRequestDto usuariosRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		List<UsuariosResponseDto> listarInfoUsuarioResponseDto= null;		  
		try {
			listarInfoUsuarioResponseDto = usuariosService.listarInfoUsuario(usuariosRequestDto.getIdentificacion());
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<List<UsuariosResponseDto>>(listarInfoUsuarioResponseDto, HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@PostMapping(value = "listarTodosLosUsuarios", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> listarTodosLosUsuarios() {
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, List> respuestaTotal = new HashMap<String, List>();
		try {
			respuestaTotal.put("respuesta", usuariosService.listarTodosLosUsuario());			
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, List>>(respuestaTotal, HttpStatus.OK);
	}
	
	@PostMapping(value = "registrarUsuario", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> registrarUsuario(@RequestBody RegistroUsuarioRequestDto registroUsuarioRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, String> respuestaTotal = new HashMap<String, String>();
		Boolean respuestaCreacionUsuario = false;
		String respuesta = "";
		
		try {
			respuestaCreacionUsuario = usuariosService.registrarUsuario(registroUsuarioRequestDto);
			respuesta = respuestaCreacionUsuario.equals(true) ? UtilitarioProperties.USUARIO_CREADO : UtilitarioProperties.USUARIO_NO_CREADO;
			respuestaTotal.put("respuesta", respuesta);
			respuestaTotal.put("status", respuestaCreacionUsuario.toString());
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, String>>(respuestaTotal, HttpStatus.OK);
	}
	
	@PostMapping(value = "actualizarUsuario", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> actualizarUsuario(@RequestBody UsuarioActualizarRequestDto usuarioActualizarRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, String> respuestaTotal = new HashMap<String, String>();
		HttpStatus httpStatus;
		try {
			if (usuarioActualizarRequestDto.getIdUsuario() != 0 && usuarioActualizarRequestDto.getEstadoUsuario() != null &&
					!usuarioActualizarRequestDto.getCorreo().equals("") && !usuarioActualizarRequestDto.getNombresCompletos().equals("") && 
					usuarioActualizarRequestDto.getIdPerfil() != 0 && usuarioActualizarRequestDto.getIdPerfilUsuario() != 0) {
				usuariosService.actualizarUsuario(usuarioActualizarRequestDto);
				respuestaTotal.put("respuesta", UtilitarioProperties.USUARIO_ACTUALIZADO_EXITO);
				respuestaTotal.put("status", "true");
				httpStatus = HttpStatus.OK;
			}else {
				respuestaTotal.put("respuesta", UtilitarioProperties.USUARIO_ACTUALIZADO_ERROR);
				respuestaTotal.put("status", "false");
				httpStatus = HttpStatus.BAD_REQUEST;
			}			
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, String>>(respuestaTotal, httpStatus);
	}
	
	@PostMapping(value = "actualizarContrasenia", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> actualizarContrasenia(@RequestBody ActualizarContraseniaRequestDto actualizarContraseniaRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, String> respuestaTotal = new HashMap<String, String>();
		HttpStatus httpStatus;
		try {
			if (actualizarContraseniaRequestDto.getIdUsuario() != 0 && 
					!actualizarContraseniaRequestDto.getContraseniaActual().equals("") && !actualizarContraseniaRequestDto.getContraseniaNueva().equals("")) {
				
				String respuestaContrasenia = usuariosService.actualizarContrasenia(actualizarContraseniaRequestDto);
				respuestaTotal.put("respuesta", respuestaContrasenia);
				respuestaTotal.put("status", "true");
				httpStatus = HttpStatus.OK;
			}else {
				respuestaTotal.put("respuesta", UtilitarioProperties.USUARIO_ACTUALIZADO_ERROR);
				respuestaTotal.put("status", "false");
				httpStatus = HttpStatus.BAD_REQUEST;
			}			
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, String>>(respuestaTotal, httpStatus);
	}
	
	@PostMapping(value = "loginUsuario", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> loginUsuario(@RequestBody LogearUsuarioRequestDto logearUsuarioRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, String> respuestaTotal = new HashMap<String, String>();
		Boolean respuestaCreacionUsuario = false;
		String respuesta = "";
		
		try {
			respuestaCreacionUsuario = usuariosService.logearUsuario(logearUsuarioRequestDto);
			respuesta = respuestaCreacionUsuario.equals(true) ? UtilitarioProperties.USUARIO_LOGEADO : UtilitarioProperties.USUARIO_NO_LOGEADO;
			respuestaTotal.put("respuesta", respuesta);
			respuestaTotal.put("status", respuestaCreacionUsuario.toString());
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, String>>(respuestaTotal, HttpStatus.OK);
	}
	
	@PostMapping(value = "infoSesionUsuario", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> infoSesionUsuario(@RequestBody InfoSesionRequestDto infoSesionRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		RespuestaInformacionSesion respuestaInformacionSesion = null;
		try {
			respuestaInformacionSesion = new RespuestaInformacionSesion(true, usuariosService.listarInformacionSesionUsuario(infoSesionRequestDto));
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaInformacionSesion>(respuestaInformacionSesion, HttpStatus.OK);
	}
	
	@PostMapping(value = "recuperarContraseniaUsuario", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> recuperarContraseniaUsuario(@RequestBody RecuperarContraseniaRequestDto recuperarContraseniaRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, Boolean> respuestaTotal = new HashMap<String, Boolean>();
		try {
			respuestaTotal.put(MENSAJE, usuariosService.recuperarContrasenia(recuperarContraseniaRequestDto));
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, Boolean>>(respuestaTotal, HttpStatus.OK);
	}
}
