package com.gob.ec.sercop.lecturadocs.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.lecturadocs.daoMongodb.BarridosDocumentosCabDao;
import com.gob.ec.sercop.lecturadocs.daoMongodb.BarridosDocumentosDetalladoDao;
import com.gob.ec.sercop.lecturadocs.dto.BarridoDocsDetResponseDto;
import com.gob.ec.sercop.lecturadocs.dto.BarridoDocumentosCabDto;
import com.gob.ec.sercop.lecturadocs.dto.BarridoDocumentosDto;
import com.gob.ec.sercop.lecturadocs.model.mongodb.BarridoDocumentosCab;
import com.gob.ec.sercop.lecturadocs.model.mongodb.BarridoDocumentosDetallado;
import com.gob.ec.sercop.lecturadocs.service.BarridoDocumentosService;

@Service
public class BarridoDocumentosServiceImpl implements BarridoDocumentosService {

	@Autowired
	BarridosDocumentosDetalladoDao barridosDocumentosDetalladoDao;
	
	@Autowired
	BarridosDocumentosCabDao barridoDocumentosCabDao;	

	@Override
	public List<BarridoDocumentosDto> findAll() {

		List<BarridoDocumentosDetallado> listaBarridoDocumentos = new ArrayList<>();
		List<BarridoDocumentosDto> listaBarridoDocumentosDto = new ArrayList<>();

		listaBarridoDocumentos = barridosDocumentosDetalladoDao.findAll();
		for (BarridoDocumentosDetallado barridoDocumentos : listaBarridoDocumentos) {
			listaBarridoDocumentosDto.add(new BarridoDocumentosDto(barridoDocumentos.getCodigo(),
					barridoDocumentos.getBuscar(), barridoDocumentos.getNombreArchivo(),
					barridoDocumentos.getTexto().replace("\n", ""), barridoDocumentos.getNumeroPaginas(),
					barridoDocumentos.getUrlsPaginas(), barridoDocumentos.getFechaRegistro()));
		}
		return listaBarridoDocumentosDto;
	}

	@Override
	public List<BarridoDocumentosCabDto> listarBarridoDocsCabAll() {
		Set<BarridoDocumentosCab> listaBarridoDocumentos;
		List<BarridoDocumentosCabDto> listaBarridoDocumentosCabDto = new ArrayList<>();

		listaBarridoDocumentos = barridoDocumentosCabDao.listarBarridosDocsCabAll()
				.stream()
				.collect(Collectors.toCollection(
					() -> new TreeSet<>(Comparator.comparing(BarridoDocumentosCab::getCodigo)))
						);
		for (BarridoDocumentosCab barridoDocumentos : listaBarridoDocumentos) {
			listaBarridoDocumentosCabDto.add(new BarridoDocumentosCabDto("", barridoDocumentos.getCodigo(), barridoDocumentos.getDescripcionCompra(), barridoDocumentos.getFechaPublicacion(), barridoDocumentos.getEstado()));
		}

		return listaBarridoDocumentosCabDto;
	}

	@Override
	public List<BarridoDocsDetResponseDto> listarBarridoDocsDetallado(String codigoProceso) {
		List<BarridoDocumentosDetallado> listaBarridoDocumentos = new ArrayList<>();
		List<BarridoDocsDetResponseDto> listaBarridoDocsDetResponseDto = new ArrayList<>();
		
		listaBarridoDocumentos = barridosDocumentosDetalladoDao.listarBarridosDocsDetallado(codigoProceso);
		for (BarridoDocumentosDetallado barridoDocumentos : listaBarridoDocumentos) {
			listaBarridoDocsDetResponseDto.add(new BarridoDocsDetResponseDto(barridoDocumentos.getCodigo(),
					barridoDocumentos.getBuscar(), barridoDocumentos.getNombreArchivo(),
					barridoDocumentos.getTexto().replace("\n", ""), barridoDocumentos.getNumeroPaginas(),
					barridoDocumentos.getUrlsPaginas(), barridoDocumentos.getFechaRegistro(),
					barridoDocumentos.getDescripcionArchivo()));
		}
		
		return listaBarridoDocsDetResponseDto;
	}
	
	

}
