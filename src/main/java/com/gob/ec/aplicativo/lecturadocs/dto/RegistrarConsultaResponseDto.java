package com.gob.ec.sercop.lecturadocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegistrarConsultaResponseDto {
	
	private String identificacionUsuario;
	
	private String nombreCompletoUsuario;
	
	private String procedimientos;
	
	private String caracteristicas;
	
	private String fechaConsulta;
	
	private String pathArchivoJustificacion;
	
	private String nombreArchivo;

}
