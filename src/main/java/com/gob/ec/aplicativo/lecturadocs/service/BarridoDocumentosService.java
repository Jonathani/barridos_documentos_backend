package com.gob.ec.sercop.lecturadocs.service;

import java.util.List;

import com.gob.ec.sercop.lecturadocs.dto.BarridoDocsDetResponseDto;
import com.gob.ec.sercop.lecturadocs.dto.BarridoDocumentosCabDto;
import com.gob.ec.sercop.lecturadocs.dto.BarridoDocumentosDto;

public interface BarridoDocumentosService {
	
	List<BarridoDocumentosDto> findAll();
	
	List<BarridoDocumentosCabDto> listarBarridoDocsCabAll();
	
	List<BarridoDocsDetResponseDto> listarBarridoDocsDetallado(String codigoProceso);
	
}
