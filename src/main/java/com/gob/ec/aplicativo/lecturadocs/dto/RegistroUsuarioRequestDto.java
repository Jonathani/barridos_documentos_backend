package com.gob.ec.sercop.lecturadocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegistroUsuarioRequestDto {
	
	private String identificacion;
	
	private String nombresCompletos;
	
	private String correo;
	
	private long idPerfil;

}
