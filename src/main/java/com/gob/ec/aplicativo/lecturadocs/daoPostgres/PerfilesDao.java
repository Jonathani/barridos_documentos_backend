package com.gob.ec.sercop.lecturadocs.daoPostgres;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.lecturadocs.model.postgres.Perfil;

@Repository
public interface PerfilesDao extends CrudRepository<Perfil, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT perfil "
			+ "FROM Perfil perfil "
			+ "where perfil.estado is :estado "
			+ "order by perfil.idPerfil asc")
	public List<Perfil> findByEstado(Boolean estado);
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT perfil "
			+ "FROM Perfil perfil "
			+ "where perfil.idPerfil = :idPerfil")
	public Perfil findByIdPerfil(long idPerfil);

}
