package com.gob.ec.sercop.lecturadocs.service;

import java.util.List;

import com.gob.ec.sercop.lecturadocs.dto.ConsultarProcesoRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.ConsultarProcesoResponseDto;

public interface ConsultarProcesosService {
	
	List<ConsultarProcesoResponseDto> listarConsultaProcesos(ConsultarProcesoRequestDto consultarProcesoRequestDto);
	
}
