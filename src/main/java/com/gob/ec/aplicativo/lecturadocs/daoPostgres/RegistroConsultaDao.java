package com.gob.ec.sercop.lecturadocs.daoPostgres;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.lecturadocs.model.postgres.RegistroConsulta;

public interface RegistroConsultaDao extends CrudRepository<RegistroConsulta, Long>{

	@Transactional(readOnly = true)
	@Query(value = "SELECT registroConsulta "
			+ "FROM RegistroConsulta registroConsulta "
			+ "order by registroConsulta.idConsulta asc")
	public List<RegistroConsulta> findAll();
}
