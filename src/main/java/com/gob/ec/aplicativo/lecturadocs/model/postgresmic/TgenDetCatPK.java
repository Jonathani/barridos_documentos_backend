package com.gob.ec.sercop.lecturadocs.model.postgresmic;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The primary key class for the tgen_det_cat database table.
 * 
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Embeddable
public class TgenDetCatPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="catalogo_id", insertable=false, updatable=false)
	private long catalogoId;

	@Column(name="detalle_id")
	private String detalleId;
	
}