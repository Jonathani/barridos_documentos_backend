package com.gob.ec.sercop.lecturadocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuariosResponseDto {	

	private long id; 
	
	private String identificacion;
	
	private String nombreCompleto;
	
	private String fechaUsuarioCreacion;
	
	private String estadoUsuario;

	private String correo;
	
	private long idPerfil;
	
	private String perfil;
	
	private long idPerfilUsuario;

	public UsuariosResponseDto(long id, String identificacion, String nombreCompleto, String fechaUsuarioCreacion,
			String estadoUsuario, String correo) {
		super();
		this.id = id;
		this.identificacion = identificacion;
		this.nombreCompleto = nombreCompleto;
		this.fechaUsuarioCreacion = fechaUsuarioCreacion;
		this.estadoUsuario = estadoUsuario;
		this.correo = correo;
	}	
	
}
