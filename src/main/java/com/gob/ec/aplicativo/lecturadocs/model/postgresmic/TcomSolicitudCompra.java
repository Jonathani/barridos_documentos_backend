package com.gob.ec.sercop.lecturadocs.model.postgresmic;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the tcom_solicitud_compra database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tcom_solicitud_compra")
@NamedQuery(name="TcomSolicitudCompra.findAll", query="SELECT t FROM TcomSolicitudCompra t")
public class TcomSolicitudCompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_soli_compra")
	private Long idSoliCompra;
	
	@Column(name="codigo")
	private String codigo;
	

}