package com.gob.ec.sercop.lecturadocs.model.postgres;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the perfil_usuario database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="perfil_usuario")
@NamedQuery(name="PerfilUsuario.findAll", query="SELECT p FROM PerfilUsuario p")
public class PerfilUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "perfil_usuario_id_seq", sequenceName = "public.perfil_usuario_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "perfil_usuario_id_seq")
	@Column(name="id_perfil_usuario")
	private long idPerfilUsuario;

	@Column(name="fecha_asignacion")
	private Timestamp fechaAsignacion;

	//bi-directional many-to-one association to Perfil
	@ManyToOne
	@JoinColumn(name="id_perfil")
	private Perfil perfil;

	//bi-directional many-to-one association to Usuario

	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;	

}