package com.gob.ec.sercop.lecturadocs.service;

import java.util.List;

import com.gob.ec.sercop.lecturadocs.dto.ActualizarPrecioRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.ConsultarProcesoRequestDto;
import com.gob.ec.sercop.lecturadocs.dto.MostrarLecturaDocumentosDto;

public interface LecturaDocumentosService {

	Boolean existeCodigoProceso(ConsultarProcesoRequestDto consultarProcesoRequestDto);
	
	List<MostrarLecturaDocumentosDto> consultarTodosLosProcesos();
	
	Boolean actualizarPrecioPorCodigoProceso(ActualizarPrecioRequestDto actualizarPrecioRequestDto);
}
