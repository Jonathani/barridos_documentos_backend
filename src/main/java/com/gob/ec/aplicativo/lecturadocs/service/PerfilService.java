package com.gob.ec.sercop.lecturadocs.service;

import java.util.List;

import com.gob.ec.sercop.lecturadocs.dto.PerfilesResponseDto;
import com.gob.ec.sercop.lecturadocs.model.postgres.Perfil;

public interface PerfilService {
	
	List<PerfilesResponseDto> listarPerfilesActivos();
	
	Perfil obtenerPerfilByIdPerfil(long idPerfil);

}
