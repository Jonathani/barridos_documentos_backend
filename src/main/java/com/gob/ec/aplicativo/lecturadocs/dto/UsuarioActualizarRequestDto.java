package com.gob.ec.sercop.lecturadocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioActualizarRequestDto {	
	
	private long idUsuario;
	
	private String nombresCompletos;
	
	private String correo;
	
	private Boolean estadoUsuario;
	
	private long idPerfil;
	
	private long idPerfilUsuario;	
	
}
