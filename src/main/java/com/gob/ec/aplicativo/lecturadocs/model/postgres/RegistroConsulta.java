package com.gob.ec.sercop.lecturadocs.model.postgres;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the registro_consulta database table.
 * 
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="registro_consulta")
@NamedQuery(name="RegistroConsulta.findAll", query="SELECT r FROM RegistroConsulta r")
public class RegistroConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "registro_consulta_id_seq", sequenceName = "public.registro_consulta_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "registro_consulta_id_seq")
	@Column(name="id_consulta")
	private long idConsulta;
	
	@Column(name="caracteristicas")
	private String caracteristicas;

	@Column(name="fecha_consulta")
	private Timestamp fechaConsulta;

	@Column(name="path_archivo_justificacion")
	private String pathArchivoJustificacion;

	@Column(name="procedmientos")
	private String procedmientos;
	
	@Column(name="nombre_archivo")
	private String nombreArchivo;

	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;	

}